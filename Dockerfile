FROM node:lts-alpine

RUN apk add --update-cache curl build-base

RUN curl -f https://get.pnpm.io/v6.16.js | node - add --global pnpm

WORKDIR /abds-app

COPY package.json pnpm-lock.yaml ./

RUN pnpm install --frozen-lockfile --prod

COPY . .

RUN pnpm build

EXPOSE 3000

CMD [ "pnpm", "start" ]
