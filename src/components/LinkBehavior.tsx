import { forwardRef } from 'react';
import {
  Link as RouterLink,
  LinkProps as RouterLinkProps,
} from 'react-router-dom';

const LinkBehavior = forwardRef<any, Omit<RouterLinkProps, `to`>>(
  (props, ref) => <RouterLink ref={ref} {...props} to="/" />,
);

export default LinkBehavior;
