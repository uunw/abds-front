import { FC } from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useMutation } from '@apollo/client';

// mui
import {
  Autocomplete,
  Stack,
  Button,
  TextField,
  Box,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';

import { ADD_RECORD } from '../../services/graphql/gql';

enum RecordType {
  ADMONISH = `ADMONISH`,
  PKS_SCORE = `PKS_SCORE`,
  PAROLE = `PAROLE`,
  ACTIVITY = `ACTIVITY`,
}

interface IRecordForm {
  name: string;
  description: string;
  studentsName: string;
  type: RecordType;
}

interface Props {
  callbackValue: (vals: IRecordForm) => void;
}

const RecordForm: FC<Props> = ({ callbackValue }) => {
  const [addRecord] = useMutation(ADD_RECORD);

  const validationSchema = yup.object({
    name: yup.string().min(1).max(128).required(),
    description: yup.string().max(4096).required(),
    studentsName: yup.string().max(1024).required(),
    type: yup.mixed<RecordType>().oneOf(Object.values(RecordType)),
  });

  const formik = useFormik<IRecordForm>({
    initialValues: {
      name: ``,
      description: ``,
      studentsName: ``,
      type: RecordType.ADMONISH,
    },
    onSubmit: async (vals, helper) => {
      const { data, errors } = await addRecord({
        variables: {
          data: {
            name: vals.name,
            description: vals.description,
            studentsName: vals.studentsName,
            recordType: vals.type,
          },
        },
      });

      if (errors) {
        console.log(errors);
      }

      // formik.resetForm();
      if (data) {
        helper.resetForm();

        callbackValue(vals);
      }
    },
    validationSchema,
  });

  return (
    <Stack>
      <Box
        component="form"
        sx={{
          '& .MuiTextField-root': { width: `auto` },
        }}
        onSubmit={formik.handleSubmit}
        autoComplete="off"
      >
        <FormControl fullWidth>
          <InputLabel id="record-lable-type">{`ชนิดของบันทึก`}</InputLabel>
          <Select
            labelId="record-label-type"
            name="type"
            value={formik.values.type}
            label="Age"
            onChange={formik.handleChange}
          >
            <MenuItem
              value={RecordType.ADMONISH}
            >{`บันทึกการว่ากล่าวตักเตือน`}</MenuItem>
            <MenuItem
              value={RecordType.PKS_SCORE}
            >{`บันทึกการตัดคะแนนความประพฤติ`}</MenuItem>
            <MenuItem
              value={RecordType.PAROLE}
            >{`บันทึกการทำทัณฑ์บน`}</MenuItem>
            <MenuItem
              value={RecordType.ACTIVITY}
            >{`บันทึกการทำกิจกรรมเพื่อประเปลี่ยนพฤติกรรม`}</MenuItem>
          </Select>
        </FormControl>

        <FormControl fullWidth>
          <TextField
            autoComplete="false"
            variant="outlined"
            name="name"
            placeholder={`ชื่อบันทึก`}
            value={formik.values.name}
            onChange={formik.handleChange}
          />
        </FormControl>

        <FormControl fullWidth>
          <TextField
            autoComplete="false"
            variant="outlined"
            name="studentsName"
            placeholder={`ชื่อนักศึกษาที่เกี่ยวข้อง`}
            value={formik.values.studentsName}
            onChange={formik.handleChange}
          />
        </FormControl>

        <FormControl fullWidth>
          <TextField
            placeholder={`คำอธิบาย`}
            multiline
            rows={5}
            name="description"
            value={formik.values.description}
            onChange={formik.handleChange}
          />
        </FormControl>

        <Button
          style={{ marginTop: `10px` }}
          variant="contained"
          type="submit"
          disabled={!formik.isValid}
        >{`เพิ่มบันทึก`}</Button>
      </Box>
    </Stack>
  );
};

export default RecordForm;
