import { FC } from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';

import { FormControl, TextField, Button } from '@mui/material';

const updateRecordSchema = yup.object({
  name: yup.string().max(40).required(),
  description: yup.string().max(2048),
  studentsName: yup.string().max(1024),
});

interface IForm {
  name: string;
  description: string;
  studentsName: string;
}

interface Props {
  record: {
    name: string;
    description: string;
    studentsName: string;
  };
  onSubmit: (vals: IForm) => void;
}

const RecordUpdateForm: FC<Props> = ({ record, onSubmit }) => {
  const formik = useFormik<IForm>({
    initialValues: {
      name: record.name,
      description: record.description,
      studentsName: record.studentsName,
    },
    onSubmit,
    validationSchema: updateRecordSchema,
  });
  return (
    <form onSubmit={formik.handleSubmit}>
      <FormControl fullWidth>
        <TextField
          name="name"
          type="text"
          label={`ชื่อบันทึก`}
          variant="outlined"
          value={formik.values.name}
          onChange={formik.handleChange}
        />
      </FormControl>

      <FormControl fullWidth>
        <TextField
          name="description"
          type="text"
          label={`คำอธิบายบันทึก`}
          variant="outlined"
          value={formik.values.description}
          onChange={formik.handleChange}
        />
      </FormControl>
      <FormControl fullWidth>
        <TextField
          name="studentsName"
          type="text"
          label={`รายชื่อนักศีกษาที่เกี่ยวข้อง`}
          variant="outlined"
          value={formik.values.studentsName}
          onChange={formik.handleChange}
        />
      </FormControl>

      <FormControl fullWidth>
        <Button
          type="submit"
          variant="contained"
          disabled={!formik.isValid}
        >{`อัพเดท`}</Button>
      </FormControl>
    </form>
  );
};

export default RecordUpdateForm;
