import { FC } from 'react';

// Material UI
import {
  List,
  ListItemButton,
  ListItemText,
  ListItemIcon,
} from '@mui/material';
// import InboxIcon from '@mui/icons-material/Inbox';
import CheckroomIcon from '@mui/icons-material/Checkroom';
import ContentCutIcon from '@mui/icons-material/ContentCut';
import DirectionsWalkIcon from '@mui/icons-material/DirectionsWalk';
import HikingIcon from '@mui/icons-material/Hiking';

interface IRecordList {
  icon: any;
  text: string;
  type: RecordType;
}

enum RecordType {
  ADMONISH = `ADMONISH`,
  PKS_SCORE = `PKS_SCORE`,
  PAROLE = `PAROLE`,
  ACTIVITY = `ACTIVITY`,
}

const recordList: IRecordList[] = [
  {
    icon: <CheckroomIcon />,
    text: `บันทึกการว่ากล่าวตักเตือน`,
    type: RecordType.ADMONISH,
  },
  {
    text: `บันทึกการตัดคะแนนความประพฤติ`,
    icon: <ContentCutIcon />,
    type: RecordType.PKS_SCORE,
  },
  {
    text: `บันทึกการทำทัณฑ์บน`,
    icon: <DirectionsWalkIcon />,
    type: RecordType.PAROLE,
  },
  {
    text: `บันทึกการทำกิจกรรมเพื่อประเปลี่ยนพฤติกรรม`,
    icon: <HikingIcon />,
    type: RecordType.ACTIVITY,
  },
];

interface Props {
  currentIndex: RecordType;
  changeIndex: (type: RecordType) => void;
}

const RecordList: FC<Props> = ({ currentIndex, changeIndex }) => {
  return (
    <List component="nav" aria-label="main mailbox folders">
      {recordList.map((r, i) => (
        <ListItemButton
          key={i}
          selected={currentIndex === r.type}
          onClick={() => changeIndex(r.type)}
        >
          <ListItemIcon>{r.icon}</ListItemIcon>
          <ListItemText
            primary={r.text}
            style={{ textOverflow: `ellipsis`, whiteSpace: `nowrap` }}
          />
        </ListItemButton>
      ))}
    </List>
  );
};

export default RecordList;
