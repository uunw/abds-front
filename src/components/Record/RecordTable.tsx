import { FC } from 'react';
import { useToggle } from '@react-hookz/web';
import { useMutation } from '@apollo/client';

import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableContainer,
  Button,
} from '@mui/material';
import { Apps as AppsIcon } from '@mui/icons-material';

import RecordActionDialog from './RecordActionDialog';
import RecordUpdateFormDialog from './RecordUpdateFormDialog';

enum ActionType {
  READ,
  UPDATE,
  DELETE,
}

interface IRecord {
  id: string;
  name: string;
  recordType: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
}

interface Props {
  records: IRecord[];
  onClickAction: () => void;
}

const RecordTable: FC<Props> = ({ records, onClickAction }) => {
  const [dialogOpen, setDialogOpen] = useToggle(false);

  const handleClickRecordAction = (id: string) => {
    setDialogOpen(true);
  };

  return (
    <TableContainer>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>{`ลำดับ`}</TableCell>
            <TableCell align="right">{`ชื่อบันทึก`}</TableCell>
            <TableCell align="right">{`คำอธิบาย`}</TableCell>
            <TableCell align="right">Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {records.map((r, i) => (
            <TableRow
              key={i}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {i}
              </TableCell>
              <TableCell align="right">{r.name}</TableCell>
              <TableCell align="right">{r.description}</TableCell>
              <TableCell align="right">
                <Button
                  variant="outlined"
                  onClick={() => handleClickRecordAction(r.id)}
                >
                  <AppsIcon />
                </Button>
              </TableCell>
              <RecordActionDialog
                open={dialogOpen}
                onClose={() => setDialogOpen(false)}
                id={r.id}
                onClickAction={() => {
                  setDialogOpen(false);
                  onClickAction();
                }}
              />
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default RecordTable;
