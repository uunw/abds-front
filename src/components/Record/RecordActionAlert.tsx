import { FC, useState } from 'react';

import { Snackbar, Alert } from '@mui/material';

interface Props {
  msg: string;
}

const RecordActionAlert: FC<Props> = ({ msg }) => {
  const [open, setOpen] = useState<boolean>(true);

  function handleClose() {
    setOpen(false);
  }
  return (
    <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
      <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
        {/* {`เพิ่มบันทึกสำเร็จ`} */}
        {msg}
      </Alert>
    </Snackbar>
  );
};

export default RecordActionAlert;
