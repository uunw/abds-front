import { FC, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useQuery, useMutation } from '@apollo/client';
import jwtDecode from 'jwt-decode';
import { useLocalstorage } from 'rooks';

import {
  Dialog,
  DialogTitle,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
} from '@mui/material';
import {
  Delete as DeleteIcon,
  Edit as EditIcon,
  Pageview as PageViewIcon,
} from '@mui/icons-material';

import RecordDataDialog from './RecordDataDialog';
import Swal from '../SweetAlert';

import { DELETE_RECORD } from '../../services/graphql/gql';

enum ActionType {
  READ,
  UPDATE,
  DELETE,
}

interface Props {
  id: string;
  open: boolean;
  onClickAction: () => void;
  onClose: () => void;
}

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer);
    toast.addEventListener('mouseleave', Swal.resumeTimer);
  },
});

const RecordActionDialog: FC<Props> = ({
  id,
  open,
  onClickAction,
  onClose,
}) => {
  const navigate = useNavigate();
  const [localUser] = useLocalstorage(`user`);
  const [_openRecordUpdateFormDialog, setOpenRecordUpdateFormDialog] =
    useState(false);
  const [openRecordData, setOpenRecordData] = useState(false);

  const [removeRecord] = useMutation(DELETE_RECORD);

  const handleListItemClick = () => {
    onClickAction();
  };

  return (
    <Dialog onClose={() => onClose()} open={open}>
      <DialogTitle>{`เลือก action ของบันทึก`}</DialogTitle>
      <List sx={{ pt: 0 }}>
        <ListItem
          autoFocus
          button
          onClick={() => navigate(`/app/record-data/${id}`)}
        >
          <ListItemAvatar>
            <Avatar>
              <PageViewIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary={`อ่านรายระเอียดบันทึก`} />
        </ListItem>

        <ListItem
          autoFocus
          button
          onClick={() => navigate(`/app/record-update/${id}`)}
        >
          <ListItemAvatar>
            <Avatar>
              <EditIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary={`แก้ไขบันทึก`} />
        </ListItem>

        {localUser.roles === `SUPERVISOR` && (
          <ListItem
            autoFocus
            button
            onClick={async () => {
              await handleListItemClick();

              const { data } = await removeRecord({
                variables: {
                  removeRecordId: id,
                },
              });

              if (data) {
                Toast.fire({
                  icon: `success`,
                  title: `ลบบันทึกเสร็จสิ้น`,
                  position: `bottom-end`,
                });
              }
            }}
          >
            <ListItemAvatar>
              <Avatar>
                <DeleteIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={`ลบบันทึก`} />
          </ListItem>
        )}
      </List>

      <RecordDataDialog
        open={openRecordData}
        onClose={() => setOpenRecordData(false)}
      />
    </Dialog>
  );
};

export default RecordActionDialog;
