import { FC } from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useMutation } from '@apollo/client';

import {
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
  Button,
  DialogContentText,
} from '@mui/material';

import { ADD_USER } from '../services/graphql/gql';
import { IAddUser } from '../services/graphql/types';

interface IForm {
  name: string;
  // email: string;
  username: string;
  password: string;
}

interface Props {
  open: boolean;
  onSubmit: () => Promise<void>;
  onClose: () => void;
}

const UserAddDialog: FC<Props> = ({ open, onSubmit, onClose }) => {
  const [addUser] = useMutation<IAddUser>(ADD_USER);

  const validationSchema = yup.object({
    // email: yup.string().email().required(),
    username: yup
      .string()
      .matches(RegExp(`^(?=[a-zA-Z0-9._]{5,20}$)(?!.*[_.]{2})[^_.].*[^_.]$`))
      .required(),
    name: yup.string().min(4).max(40).required(),
    password: yup.string().min(8).required(),
  });

  const formik = useFormik<IForm>({
    initialValues: {
      // email: ``,
      username: ``,
      name: ``,
      password: ``,
    },
    onSubmit: async (v) => {
      console.log(v);
      const { username, name, password } = v;

      await addUser({
        variables: {
          newUser: {
            // email,
            username,
            name,
            password,
          },
        },
      });

      await onSubmit();
    },
    validationSchema,
  });

  function handleClose() {
    onClose();
  }

  return (
    <Dialog open={open} onClose={handleClose}>
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle>{`เพิ่มผู้ใช้งาน`}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {`เพิ่มบัชชีผู้ใช้งานสำหรับครูปกครอง`}
          </DialogContentText>
          {/* <TextField
            autoFocus
            margin="dense"
            id="email"
            label={`ที่อยู่อีเมล`}
            type="email"
            fullWidth
            variant="standard"
            onChange={formik.handleChange}
            value={formik.values.email}
          /> */}

          <TextField
            autoFocus
            margin="dense"
            id="username"
            label={`ชื่อผู้ใช้งาน`}
            type="text"
            fullWidth
            variant="standard"
            onChange={formik.handleChange}
            value={formik.values.username}
          />

          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={`ชื่อแสดง`}
            type="text"
            fullWidth
            variant="standard"
            onChange={formik.handleChange}
            value={formik.values.name}
          />

          <TextField
            autoFocus
            margin="dense"
            id="password"
            label={`รหัสผ่าน`}
            type="password"
            fullWidth
            variant="standard"
            onChange={formik.handleChange}
            value={formik.values.password}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>{`ยกเลิก`}</Button>
          <Button
            onClick={handleClose}
            type="submit"
            disabled={!formik.isValid}
          >{`เพิ่มผู้ใช้งาน`}</Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default UserAddDialog;
