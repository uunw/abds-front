import { FC, MouseEvent, ChangeEvent } from 'react';

// Material UI
import {
  TableHead,
  TableRow,
  TableCell,
  TableSortLabel,
  Box,
  Checkbox,
} from '@mui/material';
import { visuallyHidden } from '@mui/utils';

interface Data {
  createdAt: Date;
  students: string[];
}

interface HeadCell {
  id: keyof Data;
  label: string;
  numeric: boolean;
  disablePadding: boolean;
}

const headCells: readonly HeadCell[] = [
  {
    id: `createdAt`,
    numeric: false,
    disablePadding: true,
    label: `เพิ่มล่าสุด`,
  },
  {
    id: `students`,
    numeric: true,
    disablePadding: false,
    label: `นักศึกษาที่เกี่ยวข้อง`,
  },
];

interface Props {
  numSelected: number;
  order?: `asc` | `desc`;
  orderBy?: keyof Data;
  rowCount: number;

  // Callback
  onRequestSort: (event: MouseEvent<unknown>, property: keyof Data) => void;
  onSelectAllClick: (event: ChangeEvent<HTMLInputElement>) => void;
}

const EnhancedTableHead: FC<Props> = ({
  numSelected,
  onRequestSort,
  onSelectAllClick,
  order = `asc`,
  orderBy = `createdAt`,
  rowCount,
}) => {
  const createSortHandler =
    (property: keyof Data) => (event: MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            color="primary"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              'aria-label': `select all desserts`,
            }}
          />
        </TableCell>

        {headCells.map((hc, i) => (
          <TableCell
            key={i}
            align={hc.numeric ? 'right' : 'left'}
            padding={hc.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === hc.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === hc.id}
              direction={orderBy === hc.id ? order : 'asc'}
              onClick={createSortHandler(hc.id)}
            >
              {hc.label}
              {orderBy === hc.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export default EnhancedTableHead;
