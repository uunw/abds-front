import { FC } from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';

import { FormControl, TextField, Button } from '@mui/material';

const updateUserSchema = yup.object({
  // email: yup.string().email().required(),
  username: yup
    .string()
    .matches(RegExp(`^(?=[a-zA-Z0-9._]{5,20}$)(?!.*[_.]{2})[^_.].*[^_.]$`))
    .required(),
  name: yup.string().max(40).required(),
  password: yup.string().min(8).max(255).required(),
});

interface IForm {
  // email: string;
  username: string;
  name: string;
  password: string;
}

interface Props {
  user: {
    // email: string;
    username: string;
    name: string;
  };
  onSubmit: (vals: IForm) => void;
}

const UserUpdateForm: FC<Props> = ({ user, onSubmit }) => {
  const formik = useFormik<IForm>({
    initialValues: {
      // email: user.email,
      username: user.username,
      name: user.name,
      password: ``,
    },
    validationSchema: updateUserSchema,
    onSubmit,
  });
  return (
    <form onSubmit={formik.handleSubmit}>
      <FormControl fullWidth>
        {/* <TextField
          name="email"
          type="email"
          placeholder={`ที่อยู่อีเมล`}
          value={formik.values.email}
          onChange={formik.handleChange}
        /> */}
        <TextField
          name="username"
          type="username"
          placeholder={`ชื่อผู้ใช้งาน`}
          value={formik.values.username}
          onChange={formik.handleChange}
        />
      </FormControl>
      <FormControl>
        <TextField
          name="name"
          type="text"
          placeholder={`ชื่อแสดง`}
          value={formik.values.name}
          onChange={formik.handleChange}
        />
      </FormControl>

      <FormControl>
        <TextField
          name="password"
          type="password"
          placeholder={`รหัสผ่าน`}
          value={formik.values.password}
          onChange={formik.handleChange}
        />
      </FormControl>

      <Button type="submit" variant="outlined" disabled={!formik.isValid}>
        {`แก้ไขข้อมูลผู้ใช้งาน`}
      </Button>
    </form>
  );
};

export default UserUpdateForm;
