import { FC } from 'react';
import { useRoutes, BrowserRouter } from 'react-router-dom';
import { useLocalstorage } from 'rooks';
import {
  ApolloProvider,
  ApolloClient,
  NormalizedCacheObject,
  InMemoryCache,
  useQuery,
  useLazyQuery,
} from '@apollo/client';
import useSWR from 'swr';
import jwtDecode from 'jwt-decode';

// Material UI
import {
  StyledEngineProvider,
  ThemeProvider,
  CssBaseline,
  CircularProgress,
  Backdrop,
} from '@mui/material';

import { noAuthRoutes, withAuthRoutes } from '../routes';
import CanNotConnectServer from '../pages/CanNotConnectServer';
import { PING, FIND_USER_BY_ID } from '../services/graphql/gql';
import { IFindUserByID } from '../services/graphql/types';

import theme from '../theme';

import { AppContext } from '../contexts';

const App: FC = () => {
  const [localToken] = useLocalstorage(`token`);
  // const [locaUser] = useLocalstorage(`user`);

  const { data: ping, error, loading, refetch } = useQuery(PING);

  const noAuthContect = useRoutes(noAuthRoutes);
  const withAuthContect = useRoutes(withAuthRoutes);

  return (
    // <AppContext.Provider value={contextValue}>
    <StyledEngineProvider>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={loading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>

        {ping && (localToken ? withAuthContect : noAuthContect)}

        {error && <CanNotConnectServer onReload={refetch} />}
      </ThemeProvider>
    </StyledEngineProvider>
    // </AppContext.Provider>
  );
};

export default App;
