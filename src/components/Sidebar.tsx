import { FC, useState, useEffect } from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { useLocalstorage } from 'rooks';
import jwtDecode from 'jwt-decode';

// Mui
import {
  styled,
  Drawer as MuiDrawer,
  Toolbar,
  IconButton,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  ListItemButton,
  ListSubheader,
  LinkProps,
  Divider,
  Tooltip,
} from '@mui/material';
import {
  ChevronLeft as ChevronLeftIcon,
  Dashboard as DashboardIcon,
  ShoppingCart as ShoppingCartIcon,
  BarChart as BarChartIcon,
  Layers as LayersIcon,
  People as PeopleIcon,
} from '@mui/icons-material';

import { epoch, secs } from '../utils';

interface Props {
  open: boolean;
  toggleDrawer: () => void;
}

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  '& .MuiDrawer-paper': {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: 240,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    boxSizing: 'border-box',
    ...(!open && {
      overflowX: 'hidden',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: theme.spacing(7),
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9),
      },
    }),
  },
}));

const Sidebar: FC<Props> = ({ open, toggleDrawer }) => {
  const navigate = useNavigate();

  const [tokenExpString, setTokenExpString] = useState(``);

  const [localToken, _setLocalToken, removeLocalToken] =
    useLocalstorage(`token`);
  const [localUser, _setLocalUser, removeLocalUser] = useLocalstorage(`user`);
  const payloadData = jwtDecode<{ exp: number }>(localToken);

  const countDownDate = new Date(payloadData.exp).getTime();

  // console.log(countDownDate);
  // console.log(payloadData);

  // console.log(Date.now(), payloadData.exp);

  useEffect(() => {
    if (localUser.role !== `SUPERVISOR`) {
      navigate(`/app/dashboard`);
    }

    // if (Date.now() >= payloadData.exp) {
    //   setTokenExp(true);
    // }
    const x = setInterval(() => {
      // Get today's date and time
      var now = new Date().getTime();

      // Find the distance between now and the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      // var hours = Math.floor(
      //   (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
      // );
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // setTokenExpString(`${days}d ${hours}h ${minutes}m ${seconds}s`);
      setTokenExpString(`${minutes}m ${seconds}s`);

      // If the count down is over, write some text
      if (distance < 0) {
        removeLocalToken();
        removeLocalUser();

        navigate(`/`);

        location.reload();

        return () => clearInterval(x);
        // document.getElementById('demo').innerHTML = 'EXPIRED';
      }
    }, 1000);
  }, []);
  return (
    <Drawer variant="permanent" open={open}>
      <Toolbar
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-end',
          px: [1],
        }}
      >
        <IconButton onClick={toggleDrawer}>
          <ChevronLeftIcon />
        </IconButton>
      </Toolbar>

      <Divider />

      <List>
        <ListSubheader inset>{`เมนูหลัก`}</ListSubheader>
        {/* <SideBarListItem li={listItem} /> */}

        <Tooltip title={`หน้าแรก`} placement="right" arrow>
          <ListItem component={RouterLink} to={`/app/dashboard`} button>
            <ListItemIcon>
              <DashboardIcon />
            </ListItemIcon>
            <ListItemText primary={`หน้าแรก`} />
          </ListItem>
        </Tooltip>
        <Tooltip title={`บันทึกต่างๆ`} placement="right" arrow>
          <ListItem component={RouterLink} to={`/app/record`} button>
            <ListItemIcon>
              <BarChartIcon />
            </ListItemIcon>
            <ListItemText primary={`บันทึกต่างๆ`} />
          </ListItem>
        </Tooltip>

        {localUser.roles === `SUPERVISOR` && (
          <Tooltip title={`บัญชีผู้ใช้งาน`} placement="right" arrow>
            <ListItem component={RouterLink} to={`/app/user`} button>
              <ListItemIcon>
                <PeopleIcon />
              </ListItemIcon>
              <ListItemText primary={`บัญชีผู้ใช้งาน`} />
            </ListItem>
          </Tooltip>
        )}
      </List>

      <Divider />
      <p>
        {`เหลือเวลาใช้งานอีก`} {tokenExpString}
      </p>

      {/* <List>
        <ListSubheader inset>{`ตั้งค่า`}</ListSubheader>
        <SideBarListItem li={listSettingItem} />
      </List> */}
    </Drawer>
  );
};

export default Sidebar;
