import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';

import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Link,
  Button,
} from '@mui/material';

import PaperTitle from './PaperTitle';

import { IRecentStudentRecord } from '../services/graphql/types';

interface Props {
  students: IRecentStudentRecord['recentStudentRecords'];
  onReloadRecord: () => void;
}

const RecentStudentRecord: FC<Props> = ({ students, onReloadRecord }) => {
  const navigate = useNavigate();

  const recordTypeDictionary = {
    ADMONISH: `ตักเตือน`,
    PKS_SCORE: `ตัดคะแนน`,
    PAROLE: `ทัณฑ์บน`,
    ACTIVITY: `ทำกิจกรรม`,
  };
  return (
    <>
      <PaperTitle>
        {`บันทึกที่ถูกเพิ่มล่าสุด`}
        <Button
          style={{ marginLeft: `10px` }}
          variant="outlined"
          onClick={onReloadRecord}
        >{`โหลดข้อมูลใหม่`}</Button>
      </PaperTitle>

      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>{`เวลา`}</TableCell>
            <TableCell>{`ชื่อบันทึก`}</TableCell>
            <TableCell>{`ชนิดของบันทึก`}</TableCell>
            <TableCell>{`ชื่อนักศึกษาที่เกี่ยวข้อง`}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {students.map(({ id, name, recordType, studentsName, createdAt }) => (
            <TableRow key={id}>
              <TableCell>
                {dayjs(createdAt, `th`).format(`hh:mmA DD/MM/YYYY`)}
              </TableCell>
              <TableCell>
                <Link
                  color="inherit"
                  onClick={() => {
                    navigate(`/app/record-data/${id}`);
                  }}
                >
                  {name}
                </Link>
              </TableCell>
              <TableCell>{recordTypeDictionary[recordType]}</TableCell>
              <TableCell>{studentsName}</TableCell>
              {/* <TableCell>{s.paymentMethod}</TableCell>
              <TableCell align="right">{`$${s.amount}`}</TableCell> */}
            </TableRow>
          ))}
        </TableBody>
      </Table>

      {/* <Link color="primary" onClick={() => navigate(`/`)} sx={{ mt: 3 }}>
        See more orders
      </Link> */}
    </>
  );
};

export default RecentStudentRecord;
