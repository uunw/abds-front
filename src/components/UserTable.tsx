import { FC, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';
import { useLocalstorage } from 'rooks';
import { useMutation } from '@apollo/client';

import {
  Table,
  TableHead,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Paper,
  Button,
  Menu,
  MenuItem,
} from '@mui/material';
import { Settings as SettingsIcon } from '@mui/icons-material';

// import { AppContext } from '../contexts';

import { REMOVE_USER, UPDATE_USER } from '../services/graphql/gql';

interface IUser {
  id: string;
  username: string;
  name: string;
  roles: string;
  createdAt: Date;
  updatedAt: Date;
}

interface Props {
  users: IUser[];
  onAction: () => void;
}

const UserTable: FC<Props> = ({ users }) => {
  const navigate = useNavigate();

  const [localUser] = useLocalstorage(`user`);

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const [removeUser] = useMutation(REMOVE_USER);
  // const [updateUser] = useMutation(UPDATE_USER);

  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = async () => {
    await setAnchorEl(null);
  };
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>{`ชื่อผู้ใช้`}</TableCell>
            <TableCell align="right">{`ชื่อแสดง`}</TableCell>
            <TableCell align="right">{`สร้างเมื่อ`}</TableCell>
            <TableCell align="right">{`แก้ไขเมื่อ`}</TableCell>
            {localUser.roles === `SUPERVISOR` && (
              <TableCell align="right">{`action`}</TableCell>
            )}
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map(({ id, name, username, createdAt, updatedAt }, i) => (
            <TableRow
              key={i}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {username}
              </TableCell>
              <TableCell align="right">{name}</TableCell>
              <TableCell align="right">
                {dayjs(createdAt, `th`).format(`HH:mm DD/MM/YYYY`)}
              </TableCell>
              <TableCell align="right">
                {dayjs(updatedAt, `th`).format(`HH:mm DD/MM/YYYY`)}
              </TableCell>
              <TableCell align="right">
                <Button
                  variant="outlined"
                  aria-expanded={open ? 'true' : undefined}
                  onClick={handleClick}
                >
                  <SettingsIcon />
                </Button>

                {localUser.roles === `SUPERVISOR` && (
                  <Menu
                    id="basic-menu"
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}
                    MenuListProps={{
                      'aria-labelledby': 'basic-button',
                    }}
                  >
                    <MenuItem
                      onClick={async () => {
                        await handleClose();
                        navigate(`/app/user-update/${id}`);
                      }}
                    >{`แก้ไขข้อมูล`}</MenuItem>

                    {!!localUser && localUser.id !== id && (
                      <MenuItem
                        onClick={async () => {
                          await removeUser({
                            variables: {
                              removeUserId: id,
                            },
                          });

                          await handleClose();
                        }}
                      >{`ลบผู้ใช้งาน`}</MenuItem>
                    )}
                  </Menu>
                )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default UserTable;
