import { FC, createContext } from 'react';

interface IAppContext {
  user: {
    id: string;
    // email: string;
    username: string;
    name: string;
    roles: string;
    createdAt: Date;
    updatedAt: Date;
  };
  token: string;
}

const AppContext = createContext<IAppContext | null>(null);

const AppProvider: FC = ({ children }) => {
  return <AppContext.Provider value={null}>{children}</AppContext.Provider>;
};

export { AppContext, AppProvider };
