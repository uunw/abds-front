import {
  ApolloClient,
  InMemoryCache,
  NormalizedCacheObject,
} from '@apollo/client';

const client = () => {
  const token = localStorage.getItem(`token`);

  const uri = new URL(
    String(import.meta.env.API_ENDPOINT || `http://localhost:3090/`),
  ).toString();

  return new ApolloClient<NormalizedCacheObject>({
    uri,
    cache: new InMemoryCache(),
    headers: {
      Authorization: token ? `Bearer ${token}` : ``,
    },
  });
};

export default client;
