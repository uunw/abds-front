interface IPing {
  ping: string;
}

interface IUsers {
  users: {
    id: string;
    // email: string;
    username: string;
    name: string;
    roles: string;
    createdAt: Date;
    updatedAt: Date;
  }[];
}

interface IFindUserByID {
  findUserByID: {
    id: string;
    name: string;
    // email: string;
    username: string;
    roles: string;
    createdAt: Date;
    updatedAt: Date;
  };
}

interface IAddUser {
  addUser: string;
}

interface IUpdateUser {
  updateUser: string;
}

interface ISignInResult {
  login: {
    token: string;
    user: {
      id: string;
      name: string;
      // email: string;
      username: string;
      roles: string;
    };
  };
}

enum RecordType {
  ADMONISH = `ADMONISH`,
  PKS_SCORE = `PKS_SCORE`,
  PAROLE = `PAROLE`,
  ACTIVITY = `ACTIVITY`,
}

interface IRecentStudentRecord {
  recentStudentRecords: {
    id: string;
    name: string;
    recordType: RecordType;
    studentsName: string;
    description: string;
    createdAt: Date;
    updatedAt: Date;
  }[];
}

interface IFindRecordByType {
  findRecordByType: {
    id: string;
    name: string;
    recordType: string;
    description: string;
    studentsName: string;
    createdAt: Date;
    updatedAt: Date;
  }[];
}

interface IFindRecordByID {
  findRecordByID: {
    id: string;
    name: string;
    recordType: string;
    studentsName: string;
    description: string;
    createdAt: Date;
    updatedAt: Date;
  };
}

interface IUpdateRecord {
  updateRecord: string;
}

interface IRecordDelete {
  removeRecord: string;
}

export type {
  IPing,
  IUsers,
  IFindUserByID,
  IFindRecordByID,
  IAddUser,
  IUpdateUser,
  ISignInResult,
  IRecentStudentRecord,
  IFindRecordByType,
  IUpdateRecord,
};
