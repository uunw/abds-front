import { gql } from '@apollo/client';

const PING = gql`
  {
    ping
  }
`;

const LOGIN = gql`
  mutation ($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      token
      user {
        id
        username
        name
        roles
        createdAt
        updatedAt
      }
    }
  }
`;

const USERS = gql`
  query {
    users {
      id
      username
      name
      roles
      createdAt
      updatedAt
    }
  }
`;

const FIND_USER_BY_ID = gql`
  query ($findUserByIdId: String!) {
    findUserByID(id: $findUserByIdId) {
      id
      username
      name
      roles
      createdAt
      updatedAt
    }
  }
`;

const ADD_USER = gql`
  mutation ($newUser: AddUserInput!) {
    addUser(newUser: $newUser)
  }
`;

const UPDATE_USER = gql`
  mutation UpdateUser($updateUserId: ID!, $data: UpdateUserDataInput!) {
    updateUser(id: $updateUserId, data: $data)
  }
`;

const REMOVE_USER = gql`
  mutation RemoveUser($removeUserId: ID!) {
    removeUser(id: $removeUserId)
  }
`;

const RECENT_STUDENT_RECORDS = gql`
  query RecentStudentRecords {
    recentStudentRecords {
      id
      name
      description
      recordType
      studentsName
      createdAt
      updatedAt
    }
  }
`;

const FIND_RECORD_BY_TYPE = gql`
  query FindRecordByType($recordType: String!) {
    findRecordByType(recordType: $recordType) {
      id
      name
      recordType
      studentsName
      description
      createdAt
      updatedAt
    }
  }
`;

const FIND_RECORD_BY_ID = gql`
  query FindRecordByID($findRecordByIdId: ID!) {
    findRecordByID(id: $findRecordByIdId) {
      id
      name
      recordType
      studentsName
      description
      createdAt
      updatedAt
    }
  }
`;

const ADD_RECORD = gql`
  mutation AddRecod($data: AddRecordInput!) {
    addRecord(data: $data)
  }
`;

const UPDATE_RECORD = gql`
  mutation UpdateRecord($updateRecordId: ID!, $data: UpdateRecordDataInput!) {
    updateRecord(id: $updateRecordId, data: $data)
  }
`;

const DELETE_RECORD = gql`
  mutation RemoveRecord($removeRecordId: ID!) {
    removeRecord(id: $removeRecordId)
  }
`;

export {
  PING,
  LOGIN,
  USERS,
  FIND_USER_BY_ID,
  ADD_USER,
  UPDATE_USER,
  REMOVE_USER,
  RECENT_STUDENT_RECORDS,
  FIND_RECORD_BY_TYPE,
  FIND_RECORD_BY_ID,
  ADD_RECORD,
  UPDATE_RECORD,
  DELETE_RECORD,
};
