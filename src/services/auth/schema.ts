import * as yup from 'yup';

const singInSchema = yup.object({
  // email: yup
  //   .string()
  //   .email(`รูปแบบของอีเมลไม่ถูดต้อง`)
  //   .required(`จำเป็นต้องใส่อีเมลเพื่อดำเนินการต่อ`),
  username: yup
    .string()
    .matches(RegExp(`^(?=[a-zA-Z0-9._]{5,20}$)(?!.*[_.]{2})[^_.].*[^_.]$`))
    .required(),
  password: yup
    .string()
    .min(8, `รหัสผ่านที่ต้องใส่ขั้นต่ำ 8`)
    .max(255)
    .required(`จำเป็นต้องใส่รหัสผ่านเพื่อดำเนินการต่อ`),
});

const updateUserSchema = yup.object({
  // email: yup.string().email(),
  username: yup
    .string()
    .matches(RegExp(`^(?=[a-zA-Z0-9._]{5,20}$)(?!.*[_.]{2})[^_.].*[^_.]$`))
    .required(),
  // username: yup.string().min(4).max(255),
});

const updateUserPasswordSchema = yup.object({
  password: yup.string().min(8).max(255),
  repassword: yup.string().oneOf([yup.ref(`password`), null]),
});

export { singInSchema, updateUserSchema, updateUserPasswordSchema };
