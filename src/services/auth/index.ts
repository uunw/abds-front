import { useNavigate } from 'react-router-dom';
import { useLocalstorage } from 'rooks';

function handleLogout() {
  const [_localToken, _setLocalToken, removeLocalToken] =
    useLocalstorage(`token`);
  const navigate = useNavigate();

  removeLocalToken();
  navigate(`/`);
}

export { handleLogout };
