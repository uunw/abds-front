import { RouteObject, Navigate, Outlet } from 'react-router-dom';

// Auth
import SignInPageDefault from '../pages/auth/SignInPageDefault';

// App
import DashboardLayout from '../layouts/AppLayout';

import DashboardHome from '../pages/app/DashboardPage';
import RecordPage from '../pages/app/RecordPage';

// Setting page
import SettingPage from '../pages/app/SettingPage';
import SettingProfilePage from '../pages/app/SettingProfilePage';
import UserPage from '../pages/app/UserPage';
import UserUpdatePage from '../pages/app/UserUpdatePage';

import RecordDataPage from '../pages/app/RecordDataPage';
import RecordUpdatePage from '../pages/app/RecordUpdatePage';

const noAuthRoutes: RouteObject[] = [
  {
    path: `/`,
    element: <SignInPageDefault />,
  },
  {
    path: `*`,
    element: <Navigate to="/" />,
  },
];

const withAuthRoutes: RouteObject[] = [
  {
    path: `/`,
    element: <Navigate to="/app/dashboard" />,
  },
  {
    path: `app`,
    element: <DashboardLayout />,
    children: [
      {
        path: `dashboard`,
        element: <DashboardHome />,
      },
      {
        path: `record`,
        element: <RecordPage />,
        children: [
          {
            path: `*`,
            element: <h1>asasdd</h1>,
          },
        ],
      },
      {
        path: `record-data/:id`,
        element: <RecordDataPage />,
      },
      {
        path: `record-update/:id`,
        element: <RecordUpdatePage />,
      },
      {
        path: `user`,
        element: <UserPage />,
      },
      {
        path: `user-update/:id`,
        element: <UserUpdatePage />,
      },
      {
        path: `setting`,
        element: <SettingPage />,
      },
      {
        path: `*`,
        element: (
          <div>
            <h1>not found</h1>
          </div>
        ),
      },
    ],
  },
];

export { noAuthRoutes, withAuthRoutes };
