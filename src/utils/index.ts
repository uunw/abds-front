import MD5 from './md5';
import secs from './secs';

const epoch = (date: Date) => Math.floor(date.getTime() / 1000);

export { MD5, epoch, secs };
