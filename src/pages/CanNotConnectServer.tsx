import { FC, useEffect } from 'react';

import { Container, Typography } from '@mui/material';

interface Props {
  onReload: () => void;
}

const CanNotConnectServer: FC<Props> = ({ onReload }) => {
  useEffect(() => {
    setTimeout(() => {
      onReload();
    }, 5000);
  }, []);

  return (
    <Container component="main" sx={{ mt: 8, mb: 2 }} maxWidth="sm">
      <Typography variant="h2" component="h1" gutterBottom>
        {`ไม่สามารถเชื่อมต่อกับเซิร์ฟเวอร์ได้ !!`}
      </Typography>
      <Typography variant="h5" component="h2" gutterBottom>
        {`เกิดข้อผิดพลาดบางอย่างที่ทำให้ฝั่งผู้ใช้งานไม่สามารถเชื่อมต่อกับฝั่งเซิอร์เวอร์ได้`}
      </Typography>
      <Typography variant="body1">{`โปรดแจ้งหน่วยงานที่เกี่ยวข้องเพื่อแก้ไขปัญหา`}</Typography>
    </Container>
  );
};

export default CanNotConnectServer;
