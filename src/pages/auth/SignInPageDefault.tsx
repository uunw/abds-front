import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import { useMutation, useQuery } from '@apollo/client';
import { useLocalstorage } from 'rooks';
import jwtDecode from 'jwt-decode';

// Material UI
import {
  Grid,
  Backdrop,
  CircularProgress,
  Box,
  Avatar,
  Typography,
  Stack,
  TextField,
  Button,
  Paper,
} from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

import { singInSchema } from '../../services/auth/schema';
import { LOGIN } from '../../services/graphql/gql';
import { ISignInResult } from '../../services/graphql/types';

const SignInPage: FC = () => {
  const navigate = useNavigate();
  const [_localToken, setLocalToken] = useLocalstorage(`token`);
  const [_localUser, setLocalUser] = useLocalstorage(`user`);

  interface InitValueType {
    // email: string;
    username: string;
    password: string;
  }

  const [authFunction, { error, loading }] = useMutation<ISignInResult>(LOGIN, {
    awaitRefetchQueries: true,
  });

  const formik = useFormik<InitValueType>({
    initialValues: {
      // email: ``,
      username: ``,
      password: ``,
    },
    validationSchema: singInSchema,
    onSubmit: async ({ username, password }) => {
      const { data } = await authFunction({
        variables: {
          username,
          password,
        },
      });

      if (!!data) {
        // const user = data.login.user;
        // console.log(user);

        setLocalToken(data.login.token.replace(/["]+/g, ``));
        setLocalUser(data.login.user);

        // if (!!data.login.user) {
        //   setLocalUser(JSON.stringify(user));
        // }

        navigate(`/app/dashboard`);

        setTimeout(() => {
          location.reload();
        }, 2000);
      }
    },
  });
  return (
    <Grid container component="main" sx={{ height: `100vh` }}>
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={loading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Grid
        item
        xs={false}
        sm={4}
        md={7}
        sx={{
          backgroundImage: `url(https://source.unsplash.com/random?book)`,
          backgroundRepeat: 'no-repeat',
          backgroundColor: (t) =>
            t.palette.mode === 'light'
              ? t.palette.grey[50]
              : t.palette.grey[900],
          backgroundSize: 'cover',
          backgroundPosition: 'center',
        }}
      />

      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <Box
          sx={{
            my: 8,
            mx: 4,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>

          <Typography component="h1" variant="h5">
            Sign In
          </Typography>

          {!_localToken && (
            <Box
              component="form"
              noValidate
              onSubmit={formik.handleSubmit}
              sx={{ mt: 1 }}
            >
              {/* <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label={`อีเมล`}
                name="email"
                autoComplete="email"
                autoFocus
                value={formik.values.email}
                onChange={formik.handleChange}
                error={
                  (formik.touched.email && Boolean(formik.errors.email)) ||
                  error?.graphQLErrors[0].extensions?.code ===
                    `WRONG_EMAIL_ADDRESS`
                }
                helperText={
                  (formik.touched.email && formik.errors.email) ||
                  (error?.graphQLErrors[0].extensions?.code ===
                    `WRONG_EMAIL_ADDRESS` &&
                    error?.message)
                }
              /> */}
              <TextField
                margin="normal"
                required
                fullWidth
                name="username"
                label={`ชื่อผู้ใช้`}
                type="text"
                id="username"
                autoComplete="current-username"
                value={formik.values.username}
                onChange={formik.handleChange}
                error={
                  (formik.touched.username &&
                    Boolean(formik.errors.username)) ||
                  error?.graphQLErrors[0].extensions?.code === `WRONG_USERNAME`
                }
                helperText={
                  (formik.touched.username && formik.errors.username) ||
                  (error?.graphQLErrors[0].extensions?.code ===
                    `WRONG_USERNAME` &&
                    error?.message)
                }
              />

              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label={`รหัสผ่าน`}
                type="password"
                id="password"
                autoComplete="current-password"
                value={formik.values.password}
                onChange={formik.handleChange}
                error={
                  (formik.touched.password &&
                    Boolean(formik.errors.password)) ||
                  error?.graphQLErrors[0].extensions?.code ===
                    `PASSWORD_NOT_CORRECT`
                }
                helperText={
                  (formik.touched.password && formik.errors.password) ||
                  (error?.graphQLErrors[0].extensions?.code ===
                    `PASSWORD_NOT_CORRECT` &&
                    error?.message)
                }
              />
              {/* <HCaptcha
              sitekey={`1f453297-ff15-4a85-a4cd-a6ea2de3b748`}
              onVerify={(token) => formik.setFieldValue(`captchaKey`, token)}
              languageOverride="th"
              ref={hcaptchaRef}
              size={requireCaptcha ? `normal` : `invisible`}
            /> */}
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                disabled={!formik.isValid}
              >
                {`เข้าสู่ระบบ`}
              </Button>

              {/* <Copyright /> */}
            </Box>
          )}
        </Box>
      </Grid>
    </Grid>
  );
};

export default SignInPage;
