import { FC } from 'react';
import { useParams } from 'react-router-dom';
import { useFormik } from 'formik';
import { useQuery, useMutation } from '@apollo/client';

import { Grid, Paper, FormControl, TextField } from '@mui/material';

import { FIND_USER_BY_ID, UPDATE_USER } from '../../services/graphql/gql';
import { IFindUserByID, IUpdateUser } from '../../services/graphql/types';

import UserUpdateForm from '../../components/UserUpdateForm';

const UserUpdatePage: FC = () => {
  const { id } = useParams();

  const { data, loading } = useQuery<IFindUserByID>(FIND_USER_BY_ID, {
    variables: {
      findUserByIdId: id,
    },
  });

  const [updateUser] = useMutation<IUpdateUser>(UPDATE_USER);

  return (
    <Grid container spacing={3}>
      <Grid item xs={12} md={5} lg={5}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 240,
          }}
        >
          {!!data && !loading && (
            <UserUpdateForm
              user={data.findUserByID}
              onSubmit={async (vals) => {
                await updateUser({
                  variables: {
                    updateUserId: id,
                    data: { ...vals },
                  },
                });
              }}
            />
          )}

          {!data && !loading && <h1>not found user data</h1>}
        </Paper>
      </Grid>
    </Grid>
  );
};

export default UserUpdatePage;
