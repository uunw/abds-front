import { FC, useState, useEffect } from 'react';
import { useMutation, useQuery } from '@apollo/client';

import {
  Grid,
  Paper,
  Snackbar,
  Alert,
  Backdrop,
  CircularProgress,
} from '@mui/material';

import RecordList from '../../components/Record/RecordList';
import RecordForm from '../../components/Record/RecordForm';
import RecordTable from '../../components/Record/RecordTable';
// import RecordActionAlert from '../../components/Record/RecordActionAlert';

import { FIND_RECORD_BY_TYPE, DELETE_RECORD } from '../../services/graphql/gql';
import { IFindRecordByType } from '../../services/graphql/types';

enum ActionType {
  READ,
  UPDATE,
  DELETE,
}

const RecordPage: FC = () => {
  const [alertOpen, setAlertOpen] = useState(false);
  enum RecordType {
    ADMONISH = `ADMONISH`,
    PKS_SCORE = `PKS_SCORE`,
    PAROLE = `PAROLE`,
    ACTIVITY = `ACTIVITY`,
  }

  const [recordIndex, setRecordIndex] = useState<RecordType>(
    RecordType.ADMONISH,
  );

  const { data, error, loading, refetch } = useQuery<IFindRecordByType>(
    FIND_RECORD_BY_TYPE,
    {
      variables: {
        recordType: recordIndex,
      },
    },
  );

  function handleAlertClose() {
    setAlertOpen(false);
  }

  useEffect(() => {
    setTimeout(async () => {
      await refetch();
    }, 3000);
  }, []);

  return (
    <Grid container spacing={3}>
      {/* Recent Deposits */}
      <Grid item xs={12} md={5} lg={5}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 240,
          }}
        >
          <RecordList
            changeIndex={async (i) => {
              await setRecordIndex(i);

              await refetch();
            }}
            currentIndex={recordIndex}
          />
        </Paper>
      </Grid>

      {/* Chart */}
      <Grid item xs={12} md={7} lg={7}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: `auto`,
          }}
        >
          {/* <Chart /> */}
          {/* <AdmonishForm /> */}
          <RecordForm
            callbackValue={() => {
              refetch();
              setAlertOpen(true);
            }}
          />
        </Paper>
      </Grid>

      {/* Recent Orders */}
      <Grid item xs={12}>
        <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
          {/* <CaseLists
            listCases={[
              {
                students: [`62201280018`],
                title: `asd`,
                updatedAt: new Date().toISOString(),
              },
            ]}
          /> */}
          {/* {loading && <h1>{`กำลังโหลดบันทึก`}</h1>} */}
          {!!data && !loading && (
            <RecordTable
              records={data.findRecordByType}
              onClickAction={async () => {
                await refetch();
              }}
            />
          )}
        </Paper>
      </Grid>

      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={loading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </Grid>
  );
};

export default RecordPage;
