import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import { useQuery } from '@apollo/client';

import { Grid, Paper, Typography, Link } from '@mui/material';

import PaperTitle from '../../components/PaperTitle';
// import CaseLists from '../../components/dashboard/CaseLists';
import RecentStudentRecord from '../../components/RecentStudentRecord';

import { RECENT_STUDENT_RECORDS } from '../../services/graphql/gql';
import { IRecentStudentRecord } from '../../services/graphql/types';

const DashBoardHome: FC = () => {
  const navigate = useNavigate();
  const { data, loading, error, refetch } = useQuery<IRecentStudentRecord>(
    RECENT_STUDENT_RECORDS,
  );

  return (
    <Grid container spacing={3}>
      {/* Recent Deposits */}
      <Grid item xs={12} md={4} lg={3}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 240,
          }}
        >
          <PaperTitle>{`ยอดบันทึกทั้งหมด`}</PaperTitle>
          <Typography component="p" variant="h4">
            {!data && `ไม่สามรถโหลดข้อมูลได้`}
            {data && `${data.recentStudentRecords.length} บันทึก`}
          </Typography>
          <Typography color="text.secondary" sx={{ flex: 1 }}>
            {`ทั้งหมด`}
          </Typography>
          <div>
            <Link color="primary" onClick={() => navigate(`/app/record`)}>
              {`ดูบันทึกแต่ละชนิด`}
            </Link>
          </div>
          {/* <Deposits /> */}
        </Paper>
      </Grid>

      <Grid item xs={12} md={8} lg={9}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 240,
          }}
        >
          {/* <CaseLists
            listCases={[
              {
                students: [`62201280018`],
                title: `asd`,
                updatedAt: new Date().toISOString(),
              },
            ]}
          /> */}
          {loading && `กำหลังโหลดข้อมูล`}
          {error && `ไม่สามารถโหลดข้อมูลได้!`}

          {data && (
            <RecentStudentRecord
              students={data.recentStudentRecords}
              onReloadRecord={() => refetch()}
            />
          )}

          {/* <Chart /> */}
        </Paper>
      </Grid>

      {/* Recent Orders */}
    </Grid>
  );
};

export default DashBoardHome;
