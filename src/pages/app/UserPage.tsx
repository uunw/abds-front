import { FC, useEffect, useState } from 'react';
import { useQuery, useMutation } from '@apollo/client';

import { Paper, Grid, Button } from '@mui/material';

import { USERS, ADD_USER } from '../../services/graphql/gql';
import { IUsers, IAddUser } from '../../services/graphql/types';

import UserTable from '../../components/UserTable';
import UserAddDialog from '../../components/UserAddDialog';

const UserPage: FC = () => {
  const [userAddDialog, setUserAddDialog] = useState<boolean>(false);

  const { data, error, loading, refetch } = useQuery<IUsers>(USERS);

  function handleClickAddUserButton() {
    setUserAddDialog(true);
  }

  function handelCloseAddUserDialog() {
    setUserAddDialog(false);
  }

  useEffect(() => {
    setTimeout(async () => {
      await refetch();
    }, 3000);
  }, []);

  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: `auto`,
          }}
        >
          <Button
            variant="outlined"
            onClick={handleClickAddUserButton}
          >{`เพิ่มผู้ใช้งาน`}</Button>

          {error && (
            <h1>{`เกิดข้อผิดพลาด ไม่สามารถโหลดข้อมูลผู้ใช้งานได้!!`}</h1>
          )}
          {data && (
            <UserTable
              onAction={async () => {
                await refetch();
              }}
              users={data.users}
            />
          )}
        </Paper>
      </Grid>

      <UserAddDialog
        open={userAddDialog}
        onClose={handelCloseAddUserDialog}
        onSubmit={async () => {
          await refetch();
        }}
      />
    </Grid>
  );
};

export default UserPage;
