import { FC } from 'react';
import { useParams, Link } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import dayjs from 'dayjs';

import { Grid, Paper } from '@mui/material';

import { FIND_RECORD_BY_ID } from '../../services/graphql/gql';
import { IFindRecordByID } from '../../services/graphql/types';

const RecordDataPage: FC = () => {
  const { id } = useParams();

  const { data } = useQuery<IFindRecordByID>(FIND_RECORD_BY_ID, {
    variables: {
      findRecordByIdId: id,
    },
  });
  const record = data?.findRecordByID;

  if (!record) return <h1>{`ไม่มีข้อมูลบันทึก`}</h1>;

  return (
    <Grid>
      <Grid>
        <h1>record data</h1>
        <Link to={`/app/record-update/${id}`}>{`แก้ไขข้อมูลบันทึก`}</Link>
        <ul>
          <li>{`Record ID: ${record.id}`}</li>
          <li>{`ชื่อบันทึก: ${record.name}`}</li>
          <li>{`รายชื่อนักศึกษา: ${record.studentsName}`}</li>
          <li>{`คำอธิบาย: ${record.description}`}</li>
          <li>{`สร้างเมื่อ: ${dayjs(record.createdAt, `th`).format(
            `HH:mm DD/MM/YYYY`,
          )}`}</li>
          <li>{`แก้ไขเมื่อ: ${dayjs(record.updatedAt, `th`).format(
            `HH:mm DD/MM/YYYY`,
          )}`}</li>
        </ul>
      </Grid>
    </Grid>
  );
};

export default RecordDataPage;
