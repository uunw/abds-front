import { FC } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import { useQuery, useMutation } from '@apollo/client';

import { Grid, Paper, TextField, Button } from '@mui/material';

import RecordUpdateForm from '../../components/Record/RecordUpdateForm';
import PaperTitle from '../../components/PaperTitle';

import { FIND_RECORD_BY_ID, UPDATE_RECORD } from '../../services/graphql/gql';
import { IFindRecordByID, IUpdateRecord } from '../../services/graphql/types';

interface IForm {
  name: string;
  description: string;
  // studentsName: string[];
}

const RecordUpdatePage: FC = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const { data, loading } = useQuery<IFindRecordByID>(FIND_RECORD_BY_ID, {
    variables: {
      findRecordByIdId: id,
    },
  });

  const [updateRecord] = useMutation<IUpdateRecord>(UPDATE_RECORD);

  const record = data?.findRecordByID;

  if (loading) return <h1>{`กำลังโหลดข้อมูล`}</h1>;

  return (
    <Grid container spacing={3}>
      <Grid item xs={12} md={5} lg={5}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 240,
          }}
        >
          <PaperTitle>{`แก้ไขบันทึก`}</PaperTitle>
          {record && (
            <RecordUpdateForm
              record={record}
              onSubmit={async (vals) => {
                await updateRecord({
                  variables: {
                    updateRecordId: id,
                    data: {
                      name: vals.name,
                      description: vals.description,
                      studentsName: vals.studentsName,
                    },
                  },
                });

                navigate(`/app/record`);
              }}
            />
          )}
        </Paper>
      </Grid>
    </Grid>
  );
};

export default RecordUpdatePage;
