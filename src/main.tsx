import { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import {
  ApolloClient,
  ApolloProvider,
  InMemoryCache,
  NormalizedCacheObject,
} from '@apollo/client';

import App from './components/App';

import 'sweetalert2/src/sweetalert2.scss';

const token = localStorage.getItem(`token`);

const apolloCleint = new ApolloClient<NormalizedCacheObject>({
  uri: new URL(
    String(import.meta.env.API_ENDPOINT || `http://localhost:3090/`),
  ).toString(),
  cache: new InMemoryCache(),
  headers: {
    Authorization: token ? `Bearer ${token.replace(/["]+/g, '')}` : ``,
  },
});

ReactDOM.render(
  <StrictMode>
    <BrowserRouter>
      <ApolloProvider client={apolloCleint}>
        <App />
      </ApolloProvider>
    </BrowserRouter>
  </StrictMode>,
  document.querySelector(`main`),
);
