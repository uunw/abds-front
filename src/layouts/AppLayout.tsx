import { FC, useState, MouseEvent } from 'react';
import { Outlet, useNavigate } from 'react-router-dom';
import { useLocalstorage } from 'rooks';

import {
  styled,
  Avatar,
  Box,
  Toolbar,
  Typography,
  Badge,
  Container,
  Menu,
  MenuItem,
} from '@mui/material';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';

import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import NotificationsIcon from '@mui/icons-material/Notifications';

import { handleLogout } from '../services/auth';
import { AppContext } from '../contexts';
import { MD5 } from '../utils';

import Sidebar from '../components/Sidebar';
// import Copyright from '../Copyright';

const drawerWidth = 240;

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const StyledBadge = styled(Badge)(({ theme }) => ({
  '& .MuiBadge-badge': {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: 'ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    },
  },
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DashboardLayout: FC = () => {
  const navigate = useNavigate();
  const [localUser, _setLocalUser, removeLocalUser] = useLocalstorage(`user`);

  const [userMenuAnchorEl, setuserMenuAnchorEl] = useState<null | HTMLElement>(
    null,
  );
  const [sidebarOpen, setSidebarOpen] = useState<boolean>(true);

  const [_localToken, _setLocalToken, removeLocalToken] =
    useLocalstorage(`token`);

  const userMenuOpen = Boolean(userMenuAnchorEl);

  function toggleDrawer(): void {
    setSidebarOpen(!sidebarOpen);
  }

  function userMenuHandleClick(evt: MouseEvent<HTMLButtonElement>) {
    setuserMenuAnchorEl(evt.currentTarget);
  }

  function userMenuhandleClose() {
    setuserMenuAnchorEl(null);
  }

  return (
    <Box sx={{ display: 'flex' }}>
      <AppBar position="absolute" open={sidebarOpen}>
        <Toolbar
          sx={{
            pr: '24px', // keep right padding when drawer closed
          }}
        >
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={toggleDrawer}
            sx={{
              marginRight: '36px',
              ...(sidebarOpen && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            component="h1"
            variant="h6"
            color="inherit"
            noWrap
            sx={{ flexGrow: 1 }}
          >
            Dashboard
          </Typography>
          <IconButton color="inherit" onClick={userMenuHandleClick}>
            {/* <Badge badgeContent={4} color="secondary">
              <NotificationsIcon />
            </Badge> */}
            <StyledBadge
              overlap="circular"
              anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
              variant="dot"
            >
              <Avatar
                alt={localUser.name || ``}
                // src={
                //   localUser
                //     ? `https://www.gravatar.com/avatar/${MD5(localUser.)}`
                //     : undefined
                // }
                // sr
              />
            </StyledBadge>
          </IconButton>
          <Menu
            id="basic-menu"
            anchorEl={userMenuAnchorEl}
            open={userMenuOpen}
            onClose={userMenuhandleClose}
            MenuListProps={{
              'aria-labelledby': 'basic-button',
            }}
          >
            <MenuItem>{localUser && localUser.name}</MenuItem>
            <MenuItem
              onClick={() => {
                userMenuhandleClose();

                removeLocalToken();
                removeLocalUser();

                location.reload();

                navigate(`/`);
                console.log(`remove token it`);
              }}
            >{`ออกจากระบบ`}</MenuItem>
          </Menu>
        </Toolbar>
      </AppBar>
      <Sidebar open={sidebarOpen} toggleDrawer={toggleDrawer} />
      <Box
        component="main"
        sx={{
          backgroundColor: (theme) =>
            theme.palette.mode === `light`
              ? theme.palette.grey[100]
              : theme.palette.grey[900],
          flexGrow: 1,
          height: `100vh`,
          overflow: `auto`,
        }}
      >
        <Toolbar />

        <Container maxWidth="lg" sx={{ my: 4 }}>
          <Outlet />

          {/* <Copyright /> */}
        </Container>
      </Box>
    </Box>
  );
};

export default DashboardLayout;
